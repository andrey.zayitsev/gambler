defmodule GamblerWeb.Router do
  use GamblerWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", GamblerWeb do
    pipe_through :api

    get "/", ScoringController, :get_top_users
  end
end
