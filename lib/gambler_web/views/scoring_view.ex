defmodule GamblerWeb.ScoringView do
  use GamblerWeb, :view

  alias Gambler.Responses.TopUsers
  alias Gambler.Entities.User

  def render("top_users.json", %{top_users: top_users}) do
    %TopUsers{
      top_users: users,
      last_fetch_timestamp: last_fetch_timestamp
    } = top_users

    %{
      users: render_many(users, __MODULE__, "top_user.json", as: :top_user),
      last_fetch_timestamp: last_fetch_timestamp
    }
  end

  def render("top_user.json", %{top_user: top_user}) do
    %User{
      id: id,
      points: points
    } = top_user

    %{
      id: id,
      points: points
    }
  end
end
