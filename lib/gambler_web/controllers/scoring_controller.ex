defmodule GamblerWeb.ScoringController do
  use GamblerWeb, :controller

  alias Gambler

  def get_top_users(conn, _) do
    top_users = Gambler.get_top_users()

    conn
    |> render("top_users.json", top_users: top_users)
  end
end
