defmodule Gambler do
  @moduledoc """
  Gambler keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  alias Gambler.Responses.TopUsers
  alias Gambler.Scoreboard.ScoreboardServer

  @spec get_top_users() :: TopUsers.t()
  def get_top_users(), do: ScoreboardServer.get_top_users()
end
