defmodule Gambler.Repo do
  use Ecto.Repo,
    otp_app: :gambler,
    adapter: Ecto.Adapters.Postgres,
    timeout: 60_000
end
