defmodule Gambler.Users do
  import Ecto.Query, warn: false

  alias Gambler.Repo
  alias Gambler.Entities.User

  @rescore_timeout 30_000

  @spec rescore_users() :: {integer(), nil}
  def rescore_users() do
    from(
      u in User,
      update: [
        set: [
          points: fragment("floor(random() * 101)"),
          updated_at: fragment("now() at time zone 'utc'")
        ]
      ]
    )
    |> Repo.update_all([], timeout: @rescore_timeout)
  end

  @spec get_users_with_greater_points(non_neg_integer()) :: list(User.t())
  def get_users_with_greater_points(points) do
    from(
      user in User,
      where: user.points > ^points,
      limit: 2
    )
    |> Repo.all()
  end
end
