defmodule Gambler.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Gambler.Repo,
      # Start the Telemetry supervisor
      GamblerWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Gambler.PubSub},
      # Start the Endpoint (http/https)
      GamblerWeb.Endpoint,
      Gambler.Scoreboard.RescoringScheduler,
      Gambler.Scoreboard.ScoreboardServer
      # Start a worker by calling: Gambler.Worker.start_link(arg)
      # {Gambler.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Gambler.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    GamblerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
