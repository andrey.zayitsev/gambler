defmodule Gambler.Responses.TopUsers do
  alias Gambler.Entities.User

  @enforce_keys [
    :top_users,
    :last_fetch_timestamp
  ]

  defstruct [
    :top_users,
    :last_fetch_timestamp
  ]

  @type t() :: %__MODULE__{
    top_users: list(User.t()),
    last_fetch_timestamp: DateTime.t()
  }
end
