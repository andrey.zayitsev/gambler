defmodule Gambler.Scoreboard.ScoreboardState do
  @enforce_keys [
    :min_number,
    :last_fetch_timestamp
  ]

  defstruct [
    :min_number,
    :last_fetch_timestamp
  ]

  @type t() :: %__MODULE__{
    min_number: non_neg_integer(),
    last_fetch_timestamp: DateTime.t()
  }
end
