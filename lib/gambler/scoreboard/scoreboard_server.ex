defmodule Gambler.Scoreboard.ScoreboardServer do
  @moduledoc """
  GenServer that acts both as an API for fetching top users from database
  and periodic background job that updates ruleset (min number for fetching users).
  """

  use GenServer

  require Logger

  alias Gambler.Users

  alias Gambler.Responses.TopUsers
  alias Gambler.Scoreboard.ScoreboardState

  @ruleset_update_interval_ms 60_000
  @get_top_users_timeout 30_000
  @min_number_boundary 100

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  ### API ###

  @spec get_top_users() :: TopUsers.t()
  def get_top_users() do
    GenServer.call(__MODULE__, :get_top_users, @get_top_users_timeout)
  end

  ### GenServer callbacks ###

  @impl true
  def init(_) do
    Logger.info("Starting ScoreboardServer server")

    initial_state = %ScoreboardState{
      min_number: 0,
      last_fetch_timestamp: DateTime.utc_now()
    }

    {:ok, initial_state, {:continue, :initial_ruleset_update}}
  end

  @impl true
  def handle_continue(:initial_ruleset_update, state) do
    {:noreply, init_ruleset_update(state)}
  end

  @impl true
  def handle_info(:ruleset_update, state) do
    {:noreply, init_ruleset_update(state)}
  end

  @impl true
  def handle_call(:get_top_users, _from, state) do
    Logger.info("Fetching top users")

    %ScoreboardState{
      min_number: min_number,
      last_fetch_timestamp: last_fetch_timestamp
    } = state

    top_users = Users.get_users_with_greater_points(min_number)

    Logger.debug("Fetched top users - #{inspect(top_users)}")

    reply = %TopUsers{top_users: top_users, last_fetch_timestamp: last_fetch_timestamp}
    new_state = %ScoreboardState{state | last_fetch_timestamp: DateTime.utc_now()}

    {:reply, reply, new_state}
  end

  ### Private util functions ###

  @spec init_ruleset_update(ScoreboardState.t()) :: ScoreboardState.t()
  defp init_ruleset_update(%ScoreboardState{} = state) do
    schedule_ruleset_update()

    %ScoreboardState{state | min_number: get_new_min_number()}
  end

  @spec schedule_ruleset_update() :: reference()
  defp schedule_ruleset_update() do
    Logger.info("Scheduling ruleset update to run in #{@ruleset_update_interval_ms} ms")
    Process.send_after(self(), :ruleset_update, @ruleset_update_interval_ms)
  end

  @spec get_new_min_number() :: non_neg_integer()
  defp get_new_min_number() do
    new_min_number = :rand.uniform(@min_number_boundary + 1) - 1
    Logger.info("New generated new min number value - #{new_min_number}")
    new_min_number
  end
end
