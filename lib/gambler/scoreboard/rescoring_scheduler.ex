defmodule Gambler.Scoreboard.RescoringScheduler do
  @moduledoc """
  GenServer that acts as a periodic background job that rescores
  user scores in database.
  """

  use GenServer

  require Logger

  alias Gambler.Users

  @rescoring_interval_ms 60_000

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  ### GenServer callbacks ###

  @impl true
  def init(_) do
    Logger.info("Starting RescoringScheduler server")

    {:ok, %{}, {:continue, :initial_rescoring}}
  end

  @impl true
  def handle_continue(:initial_rescoring, state) do
    init_new_rescoring_cycle()
    {:noreply, state}
  end

  @impl true
  def handle_info(:rescore, state) do
    init_new_rescoring_cycle()
    {:noreply, state}
  end

  ### Private util functions ###

  @spec init_new_rescoring_cycle() :: :ok
  defp init_new_rescoring_cycle() do
    schedule_rescoring()
    rescore_users()
  end

  @spec schedule_rescoring() :: reference()
  defp schedule_rescoring() do
    Logger.info("Scheduling rescoring to run in #{@rescoring_interval_ms} ms")
    Process.send_after(self(), :rescore, @rescoring_interval_ms)
  end

  @spec rescore_users() :: :ok
  defp rescore_users() do
    Logger.info("Rescoring users in database")
    Users.rescore_users()
    Logger.info("Users are rescored")
  end
end
