defmodule Gambler.Scoring.RescoringSchedulerTest do
  use Gambler.DataCase

  alias Gambler.UserFixtures

  alias Gambler.Repo
  alias Gambler.Entities.User
  alias Gambler.Scoreboard.RescoringScheduler

  describe "Rescoring scheduler tests" do
    test ":rescore event schould rescore user scores in database" do
      ### Arrange

      # Quite ugly way to test internals of gen_server
      %User{
        id: user_id,
        points: user_initial_points,
        updated_at: user_initial_updated_at
      } = UserFixtures.create_user(%{
        points: -1,
        updated_at: DateTime.utc_now() |> DateTime.add(-10, :second) |> NaiveDateTime.truncate(:second)
      })

      ### Act
      {:noreply, _} = RescoringScheduler.handle_info(:rescore, %{})

      ### Assert
      %User{
        updated_at: user_after_rescore_updated_at,
        points: user_points_after_rescore
      } = Repo.get!(User, user_id)

      # Check that 'updated_at' field was updated
      assert NaiveDateTime.compare(
        user_after_rescore_updated_at,
        user_initial_updated_at
      ) == :gt
      # Check that randomiser set new positive values in 'points' field
      assert user_initial_points != user_points_after_rescore
    end
  end
end
