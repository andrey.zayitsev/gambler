defmodule Gambler.Scoring.ScoreboardServerTest do
  use ExUnit.Case, async: true

  alias Gambler.Scoreboard.ScoreboardServer
  alias Gambler.Scoreboard.ScoreboardState

  describe "Scoreboard server tests" do
    test ":ruleset_update event should randomize min_number field in server state" do
      ### Arrange
      initial_min_number = -1
      initial_state = %ScoreboardState{
        min_number: initial_min_number,
        last_fetch_timestamp: DateTime.utc_now()
      }

      ### Act
      {:noreply, new_state} = ScoreboardServer.handle_info(:ruleset_update, initial_state)

      ### Assert
      %ScoreboardState{
        min_number: new_min_number,
        last_fetch_timestamp: last_fetch_timestamp
      } = new_state

      assert last_fetch_timestamp
      assert new_min_number
      assert new_min_number >= 0 and new_min_number <= 100
    end
  end
end
