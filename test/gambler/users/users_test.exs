defmodule Gambler.UsersTest do
  use Gambler.DataCase

  alias Gambler.UserFixtures

  alias Gambler.Repo
  alias Gambler.Entities.User
  alias Gambler.Users

  describe "Users context tests" do
    test "rescore_users/0 should generate new value for 'points' field of each user and update 'updated_at' field" do
      ### Arrange

      # Initial points will be set to -1 so we can test randomiser
      # which can produce only positive integers
      #
      # We will also force our own updated_at value that is shifted to some time in the past.
      # It is done because after rescoring we want to make sure that this field is updated.
      %User{
        id: user_id,
        points: user_initial_points,
        updated_at: user_initial_updated_at
      } = UserFixtures.create_user(%{
        points: -1,
        updated_at: DateTime.utc_now() |> DateTime.add(-10, :second) |> NaiveDateTime.truncate(:second)
      })

      ### Act
      Users.rescore_users()

      ### Assert
      %User{
        updated_at: user_after_rescore_updated_at,
        points: user_points_after_rescore
      } = Repo.get!(User, user_id)

      # Check that 'updated_at' field was updated
      assert NaiveDateTime.compare(
        user_after_rescore_updated_at,
        user_initial_updated_at
      ) == :gt
      # Check that randomiser set new positive values in 'points' field
      assert user_initial_points != user_points_after_rescore
    end
  end

end
