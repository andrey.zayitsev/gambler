defmodule Gambler.ScoringControllerTest do
  use GamblerWeb.ConnCase

  alias Gambler.UserFixtures
  alias Gambler.Entities.User

  @beyond_boundary_value_for_user_points 101

  describe "Scoring domain" do
    test "GET '/' ACTION: get_top_users, should return empty list of top users", %{conn: conn} do
      conn = get(conn, Routes.scoring_path(conn, :get_top_users))

      assert %{
        "last_fetch_timestamp" => last_fetch_timestamp,
        "users" => []
      } = json_response(conn, 200)

      assert last_fetch_timestamp
    end

    test "GET '/' ACTION: get_top_users, should return single top user", %{conn: conn} do
      %User{
        id: user_id,
        points: user_points
      } = UserFixtures.create_user(%{
        points: @beyond_boundary_value_for_user_points
      })

      conn = get(conn, Routes.scoring_path(conn, :get_top_users))

      assert %{
        "last_fetch_timestamp" => last_fetch_timestamp,
        "users" => top_users
      } = json_response(conn, 200)

      assert last_fetch_timestamp
      assert [
        %{
          "id" => ^user_id,
          "points" => ^user_points
        }
      ] = top_users
    end
  end
end
