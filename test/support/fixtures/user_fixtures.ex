defmodule Gambler.UserFixtures do
  alias Gambler.Repo
  alias Gambler.Entities.User

  def create_user(fields_override) do
    %User{}
    |> Ecto.Changeset.change(fields_override)
    |> Repo.insert!()
  end
end
