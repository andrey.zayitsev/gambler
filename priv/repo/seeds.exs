defmodule Gambler.Seeds do
  require Logger

  @num_of_generated_users 1_000_000
  @batch_size 1_000

  def seed_users() do
    if users_already_exist?() do
      Logger.info("User table is not empty - skipping seeds")
    else
      Logger.info("Seeding users")
      generate_users()
    end
  end

  defp generate_users() do
    # Streams could be used here, but I decided not to overcomplicate seeds
    1..@num_of_generated_users
    |> Enum.map(&generate_raw_user/1)
    |> Enum.chunk_every(@batch_size)
    |> Enum.each(&batch_insert/1)
  end

  defp users_already_exist?(), do: Gambler.Repo.aggregate(Gambler.Entities.User, :count) > 0

  defp generate_raw_user(_index), do: %{
    inserted_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second),
    updated_at: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
  }

  defp batch_insert(users_batch) do
    Logger.info("Inserting #{@batch_size} users")
    Gambler.Repo.insert_all(Gambler.Entities.User, users_batch)
  end
end

seed_users()
