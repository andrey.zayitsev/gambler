# Gambler - Remote.com home assignment

## Tradeoffs & Concerns

Although it was mentioned in requirements to only have one GenServer that acts both as an API and score updater,
I decided to not to strictly follow that requirement (in real life case, I would, of course, discuss it with the team first) because of following reasons:

1. Increased fault-tolerance - having two separate and decoupled GenServers, we guarantee that any instability with score updating won't impact the availability of our API. 
2. Single responsibility - it is much easier to read and maintain GenServers if the scope of their responsibility is reduced to minimum

However, such decoupling comes with a price - you can note that update of min_number and update of user scores is done in separate GenSevers with different schedulers. This might prove to be problematic, given more specific constraints.

## Requirements:

1. OTP: >= 23
2. Elixir: >= 1.13
3. Postgres: >= 11
4. Postgres instance running on port 5432

## Description:

Usually you want to describe what your application does, but I'm not sure that I'm allowed
to disclose details so I'll leave it blank.

## Local run:

1. Install dependencies:
```
mix deps.get
```
2. Create database, run migrations and seed your database with test data. This might take a while:
```
mix ecto.setup
```
3. Start local instance:
```
mix phx.server
```

Now you can test your local instance by making GET request to root endpoint:
```
> curl http://localhost:3000/
{
    "last_fetch_timestamp":null,
    "users":[
        {"id":"18603642-7553-4ae1-964e-77ec5e4b22ec","points":97},
        {"id":"954c5c48-51ef-40c3-a0a5-e3a2901a456a","points":58}
    ]
}
```
